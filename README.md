# docker-exercises

## Description
Resolución de los ejercicios para la asignatura de PG_P1_A_Principios y herramientas de desarrollo [2021/22]. 
Cada carpeta tiene la resolución de una serie de ejercicios. Cada ejercicio tiene un archivo "md", normalmente
llamado "answer_exercise_1.md" y a parte, si necesita de documentación o archivos anexos al ejercicio se encontrara
una carpeta con el mismo nombre del "md" correspondiente a ese ejercicio "answer_exercise_1/".

## Authors and acknowledgment
Author -- Alexander Gonzalez Ferreiro

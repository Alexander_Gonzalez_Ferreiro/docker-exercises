4. [2,5  ptos]  Crea  una  imagen  docker  a  partir  de  un  Dockerfile.  Esta  aplicación 
    expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK 
    para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema. 
 
    El healthcheck deberá parametrizarse con la siguiente configuración: 
 
    • La prueba se realizará cada 45 segundos 
    • Por  cada  prueba  realizada,  se  esperará  que  la  aplicación  responda  en 
    menos  de  5  segundos.  Si  tras  5  segundos  no  se  obtiene  respuesta,  se 
    considera que la prueba habrá fallado 
    • Ajustar el tiempo de espera de la primera prueba (Ejemplo: Si la aplicación 
    del contenedor tarda en iniciarse 10s, configurar el parámetro a 15s) 
    • El  número  de  reintentos  será  2.  Si  fallan  dos  pruebas  consecutivas,  el 
    contenedor deberá cambiar al estado “unhealthy”)

    - En la carpeta "answer_exercise_4/" se encuentra el archivo Dockerfile y el documento
    .pdf con todas las capturas del proceso.

    - Primeramente hemos creado un archivo Dockerfile en el que se le ha especificado lo siguiente:

    # Imagen base del contenedor a partir de la cual se crea el contenedor.
    FROM nginx:1.19.3-alpine
    # Actualizamos los paquetes instalados en la imagen y añadimos el paquete "curl".
    RUN apk update && apk add curl
    # Exponemos el puerto 8080 del contenedor al host.
    EXPOSE 8080
    # Ejecutamos el comando HEALTHCHECK que realizara los tests con los tiempos especificados.
    HEALTHCHECK --interval=45s --timeout=5s --start-period=15s --retries=2 CMD curl http://localhost/index.html || exit 1

    - En la instrucción HEALTHCHECK  especificamos diversos atributos que paso a comentar:
        a) "--interval": Intervalo cada cuanto se ejecutan los tests.
        b) "--timeout": Tiempo desde que se lanza el test hasta que se considera fallo.
        c) "--start-period": Los tests que fallen en este periodo de tiempo no seran contabilizados como fallo.
        d) "--retries": Numero de tests fallidos hasta que se considere como unhealth el estado del contenedor.
        e) Seguidamente a estos atributos se le escribe el tipo de test a pasar.

    - Una vez creado el archivo Dockerfile ejecutamos el siguiente comando: 
    -> docker build -t nginx-healthcheck .
    Que creara la imagen base, y seguidamente ejecutamos el siguiente comando para levantar el contenedor:
    -> docker run -d -p 8080:80 --name nginx-healthcheck nginx-healthcheck
    Por último, ejecutaremos el siguiente comando para revisar el estado del contenedor:
    -> docker ps -a

    - En el archivo .pdf de la carpeta "answer_exercise_4/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.
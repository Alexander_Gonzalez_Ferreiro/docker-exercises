3. [2,5 ptos] Crea un contenedor con las siguientes especificaciones: 
    a. Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3 
    b. Al  acceder  a  la  URL  localhost:8080/index.html  aparecerá  el  mensaje 
    HOMEWORK 1 
    c. Persistir el fichero index.html en un volumen llamado static_content

    - En la carpeta "answer_exercise_3/" se encuentra el archivo Dockerfile y el documento
    .pdf con todas las capturas del proceso.

    - Primeramente hemos creado un archivo Dockerfile en el que se le ha especificado lo siguiente:

    # Imagen base del contenedor a partir de la cual se crea el contenedor.
    FROM nginx:1.19.3-alpine
    # Copiamos un archivo index.html dentro del contenedor en la ruta especificada.
    COPY index.html /usr/share/nginx/html/
    # Exponemos el puerto 8080 del contenedor al host.
    EXPOSE 8080

    Ejecutamos el siguiente comando para crear la imagen resultante del Dockerfile:
    -> docker build -t nginx3:1.0 .
    Y con el siguiente comando levantamos el contenedor:
    -> docker run -d -p 8080:80 nginx3:1.0
    
    - Para el apartado "b", ya dentro del Dockerfile copiamos en el path "/usr/share/nginx/html/" un 
    archivo index.html modificado con el mensaje Homework 1 para que al acceder a la URL "http://localhost:8080/index.html"
    nos aparezca el mensaje mencionado.

    - Para el apartado "c", si acabado el apartado anterior salimos de la consola con "exit" y el contendor se cae o lo bajamos y lo volvemos a 
    levantar, veremos que todo lo que hemos echo no quedará guardado. Por lo tanto hay que crear un volumen y persistir la informacion.
    Primeramente creamos el volumen con el comando: 
    -> docker volume create static_content.
    En el segundo paso ejecutamos la instrucción: 
    -> docker run -d -p 8080:80 -v static_content:/static_content nginx3:1.0
    Ahora, volvemos a tener levantado el contenedor pero con el volumen static_content en la ruta /static_content. El siguiente paso es entrar 
    en el contenedor y copiar el index html en el volumen con los siguientes comandos:
    -> docker exec -it <containerID> sh
    -> cp /usr/share/nginx/html/index.html /static_content/
    Ejecutamos: -> ls static_content/ y veremos que el archivo index.html se ha copiado en nuestro volumen static_content. De esta forma, siempre
    tendremos disponible el documento en el volumen aunque el contendor caida, falle o lo bajemos.

    - En el archivo .pdf de la carpeta "answer_exercise_3/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.

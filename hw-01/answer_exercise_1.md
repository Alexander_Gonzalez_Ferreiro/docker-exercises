1. [1 pto] Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile).
    - Primeramente, ENTRYPOINT especifica un comando que siempre se ejecutará cuando se inicie 
    el contenedor. Por defecto será /bin/sh -c.
    CMD especifica los argumentos que se enviarán al ENTRYPOINT.

    La primera diferencia que encontramos es que el CMD se puede definir varias veces en un 
    DOCKERFILE, en cambio, ENTRYPOINT solo se podrá definir una vez.
    En sí, CMD actúa sobre el ENTRYPOINT especificado en el DOCKERFILE. Además, cada DOCKERFILE
    deberá especificar uno de los dos comandos mínimo.

    ENTRYPOINT deberá definirse cuando se use el contenedor como un ejecutable al que le 
    podremos pasar argumentos desde consola.
    En canvio, CMD se usará como una forma de definir los argumentos predeterminados para el 
    ENTRYPOINT o para ejecutar un comando dentro del contenedor.

    En conclusión, el ENTRYPOINT es él binario o punto de entrada sobre los que se ejecutarán los
    comandos, en canvio, CMD serán los comandos que se ejecutarán sobre el ENTRYPOINT, que en 
    caso de no estar definido se cogerá el que está definido por defecto.
    Además, se tiene que definir uno por lo menos dentro del DOCKERFILE, y en caso de solo definir,
    el ENTRYPOINT le pasaremos los comandos a través de argumentos por consola.
    Como última aclaración, CMD define los comandos o parámetros por defecto, los cuales pueden ser 
    sobreescritos desde la línea de comandos cuando el contenedor de docker esta corriendo.
    Y el ENTRYPOINT configura un contenedor que correrá como un ejecutable, es decir, configura un 
    "punto de entrada" o origen desde donde se ejecutarán los comandos.
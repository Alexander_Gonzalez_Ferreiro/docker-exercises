2. [1 pto] Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile).
    - A primera vista estos dos comandos realizan la misma función, copian un fichero 
    desde una localización origen a una localización destino. Pero, ambas instrucciones
    presentan un par de diferencias.
    
    Primeramente, la instrucción COPY copia un fichero de un origen a un destino solo en
    el ámbito local de la máquina. Es decir, solo se podrán copiar archivos desde un 
    origen local a un destino de la maquina.
    En cambio, la instrucción ADD nos permitirá copiar archivos desde un destino local o 
    una url a nuestra máquina o contenedor. Además, esta instrucción nos permitirá entrar
    o extraer directamente archivos comprimidos en el directorio local. Para hacer esta
    misma acción deberíamos correr un "curl" del fichero a descargar y un RUN para 
    descomprimirlo.
    Sin embargo, el comando ADD presenta una desventaja, cuando se descarga el archivo 
    comprimido, lo expande en el contenedor y como resultado el tamaño de nuestro contenedor
    aumenta. El paso similar, sería descargarlo con un curl o wget y posteriormente
    borrarlo.

    Preferiblemente, y buscando que nuestro contenedor ocupe lo mínimo posible, usaremos
    antes el comando COPY que el comando ADD, ya que el comando ADD al descomprimir los 
    archivos aumentará el tamaño de nuestros contenedores.
    
    En conclusión, si el uso que le vamos a dar es similar, usaremos antes el comando COPY
    que el comando ADD en la mayoría de casos, pero en casos específicos como copiar archivos
    desde una URL o casos en que tras un análisis previo veamos que nos es más útil, habrá 
    que analizar si realmente el comando ADD nos ayuda más que el COPY y sopesar las 
    desventajas que ello conlleva.
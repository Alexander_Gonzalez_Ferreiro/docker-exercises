5. [3 ptos] (Caso de uso) La compañía para la que trabajáis estudia la posibilidad de 
    incorporar a nivel interno una herramienta para la monitorización de logs. Para 
    ello, os han encomendado la tarea de realizar una “Proof of Concept” (PoC). Tras 
    evaluar diferentes productos, habéis considerado que una buena opción es la 
    utilización del producto Elastic stack, cumple con los requisitos y necesidades de 
    la empresa.  
 
    Tras  comentarlo  con  el  CTO  a  última  hora  de  la  tarde,  os  ha  solicitado  que 
    preparéis una presentación para mañana a primera hora. Dado el escaso margen 
    para montar la demostración, la opción más ágil y rápida es utilizar una solución 
    basada  en  contenedores  donde  levantaréis  el  motor  de  indexación 
    (ElasticSearch) y la herramienta de visualización (Kibana).

    - En la carpeta "answer_exercise_5/" se encuentra el archivo docker compose y el documento
    .pdf con todas las capturas del proceso.

    - El primer paso ha sido rellenar el archivo de configuracion "docker-compose.yml" con las 
    configuraciones para levantar dos contenedores, uno con ElasticSearch y otro con Kibana. El 
    container con Kibana se levanta una vez el de ElasticSearch se ha levantado. Además, se han 
    seguido las pautas del documento que se ha facilitado con el ejercicio y dentro de este fichero
    se puede observar toda la configuración seguida.

    Una vez acabada la configuracion se ha ejecutado el comando:
    -> docker-compose up
    Este comando ha levantado los containers necesarios para ejecutar el sistema de logs. Una vez 
    levantados los contenedores, hemos accedido a la URL "http://localhost:5601" y una vez alli 
    se ha procedido a la configuración del sistema de logs y la dashbord que nos facilita el monitorage
    de estos.

    - En el archivo .pdf de la carpeta "answer_exercise_5/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.